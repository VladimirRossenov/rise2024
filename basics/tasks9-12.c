#include <stdio.h>
#include <string.h>

void task9();
void task10();
void task11();
void task12();

int main()
{
    task9();
    task10();
    task11();
    task12();
}

void task9()
{
    char input[11];
    char toPrint = 'z';
    for (int i = 0; i < 10; ++i)
    {
        scanf("%c", &input[i]);
        if(toPrint > input[i]){
            toPrint = input[i];
        }
    }
    printf("%c\n", toPrint);
}

void task10()
{
    char string[256];
    scanf("%s", string);
    printf("The encrypted string is : ");
    for (int i = 0; i < strlen(string); ++i)
    {
        printf("%c", string[i] + 1);
    }
    printf("\nThe decrypted string is : %s\n", string);
}

void task11()
{
    unsigned short cnt = 0;
    for (int i = 2; i <= 1000; ++i)
    {
        for (int j = 1; j < i; ++j)
        {
            if (0 == i % j)
            {
                cnt++;
            }
            if (cnt > 2)
            {
                break;
            }
        }
        if (1 == cnt)
        {
            printf("%d ", i);
        }
        cnt = 0;
    }
    printf("\n");
}

void task12()
{
    double a = 1;
    while (10.0 >= a)
    {
        printf("%.1f ", a);
        a += 0.1;
    }
    printf("\n");
}