#include <stdio.h>

void task5();
void task6();
void task7();
void task8();

int main()
{
    task5();
    task6();
    task7();
    task8();
}

void task5()
{
    int a;
    scanf("%d", &a);
    if (0 == a % 2)
    {
        printf("The number %d is even.\n", a);
    }
    else
    {
        printf("The number %d is odd.\n", a);
    }
}

void task6()
{
    for (int i = 1; i <= 100; ++i)
    {
        printf("%d ", i);
    }
    printf("\n");
}

void task7()
{
    for (int i = 1; i <= 100; ++i)
    {
        if (0 == i % 17)
        {
            printf("%d ", i);
        }
    }
    printf("\n");
}

void task8()
{
    int n;
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i)
    {
        printf("N = %d, N^2 = %d, N^3 = %d ", i, i * i, i * i * i);
        printf("\n");
    }
    printf("\n");
}