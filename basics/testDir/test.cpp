#include <iostream>
#include <string>
#include <cstring>

int main(int argc, char *argv[])
{
    char dash = '-';
    for (int i = 0; i < argc; ++i)
    {
        if (strcmp(argv[i], "-r") == 0)
        {
            for (int j = argc - 1; j > i; j--)
            {
                std::cout << argv[j] << '\n';
            }
            break;
        }   
        else
        {
            std::cout << argv[i] << '\n';
        }
    }
    std::string s;
    std::cin >> s;
    for(int i = s.size() - 1; i >=0; i--){
        std::cout << s[i];
    }
    return 0;
}
