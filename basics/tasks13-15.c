#include <stdio.h>
#include <string.h>

void task13();
void task14();
void task15();

int main()
{
    task13();
    task14();
    task15();
}

void task13()
{
    char string[256];
    scanf("%s", &string);
    for (int i = strlen(string) - 1; i >= 0; --i)
    {
        printf("%c", string[i]);
    }
    printf("\n");
}

void task14()
{
    int a;
    scanf("%d", &a);
    if (0 > a)
    {
        printf("Smaller than zero\n");
    }
    else if (0 == a)
    {
        printf("Equal to zero\n");
    }
    else
    {
        printf("Bigger than zero\n");
    }
}

void task15()
{
    char c;
    getchar();
    scanf("%c", &c);
    if(48 <= c && 57 >= c){
        printf("Number");
    }
    else if(65 <= c && 90 >= c){
        printf("Capital Letter");
    }
    else if(97 <= c && 122 >= c){
        printf("Small letter");
    }
    else{
        printf("Other");
    }
    printf("\n");
}
