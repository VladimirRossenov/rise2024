#include <stdio.h>

void task1();
void task2();
void task3();
void task4();

int main()
{
    task1();
    task2();
    task3();
    task4();
}

void task1(){
    int a;
    scanf("%d", &a);
    printf("The volume of the cube is %d\n", a * a * a);
}

void task2(){
    printf("One year has %d seconds.\n", 365 * 24 * 60 * 60);
}

void task3(){
    #define convertRate 1.75
    double usd;
    scanf("%lf", &usd);
    printf("%.3lf USD is equal to %.3lf BGN.\n", usd, usd * convertRate);
    #undef convertRate
}

void task4(){
    #define convertRate 0.17
    double weight;
    scanf("%lf", &weight);
    printf("A person that weighs %.3lf on earth weighs %.3lf on the moon.\n", weight, weight * convertRate);
    #undef convertRate
}