#include <iostream>
#include <vector>
#include <algorithm>

int sumOfEls(const std::vector<int> &machines)
{
    int res = 0;
    for (const int &el : machines)
    {
        res += el;
    }
    return res;
}

int findMinMoves(std::vector<int> &machines)
{
    int sum = sumOfEls(machines);
    if (sum % machines.size() != 0)
        return -1;
    int ideal = sum / machines.size();
    int curr = 0, count = 0;
    for (int i = 0; i < machines.size(); ++i)
    {
        curr += machines[i] - ideal;
        count = std::abs(curr) > count ? std::abs(curr) : count;
        count = machines[i] - ideal > count ? machines[i] - ideal : count;
    }
    return count;
}

int main()
{
    std::vector<int> vec = {0, 0, 0, 4};
    int moves = findMinMoves(vec);
    std::cout << moves;
}