#include <iostream>
#include <cstring>

int *g_iPtr;

int main()
{
    g_iPtr = new int{15};

    int *iPtr = new int[10];
    for (int i = 0; i < 10; ++i)
    {
        iPtr[i] = i;
    }

    iPtr[12] = 10;
    std::cout << iPtr[12] << '\n';
    std::cout << iPtr[1200] << '\n';

    int *iPtr2 = new int[10]{};

    delete iPtr2;
    int b;
    std::cout << b << '\n';
}