#include <stdio.h>
#include <pthread.h>

int a = 0;

void *thread(void *arg){
	int val= (int)arg;
	a += val;;
}

int main(int argc, const char** argv)
{
	pthread_t t1, t2;
	int arg1 = 1;
	int arg2 = 2;
	pthread_create(&t1, NULL, *thread, (void*)arg1);
	pthread_create(&t2, NULL, *thread, (void*)arg2);
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
}
