#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

pthread_mutex_t l1, l2;

struct arg_st{
	pthread_mutex_t first_m;
	pthread_mutex_t second_m;
};

void *thread(void *argument_s){
	struct arg_st *args = argument_s;
	pthread_mutex_lock(&args->first_m);
	sleep(100);
	pthread_mutex_lock(&args->second_m);
	pthread_mutex_unlock(&args->first_m);
	pthread_mutex_unlock(&args->second_m);
}

int main(int argc, const char** argv)
{
	pthread_t t1, t2;
	struct arg_st args1 = {l1, l2};
	struct arg_st args2 = {l2, l1};
	pthread_create(&t1, NULL, *thread, (void*)&args1);
	pthread_create(&t2, NULL, *thread, (void*)&args2);
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
}
