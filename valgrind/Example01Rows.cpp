#include <iostream>

const int ROWS = 10000;
const int COLS = 10000;

int main() {
    int * matrix = new int[ROWS * COLS];

    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLS; ++j) {
            matrix[COLS * i + j] = i * j;
            if ((i * j) % 5 == 0) {
                matrix[COLS * i + j] = (i * j) % 5;
            }
        }
    }

    delete[] matrix;
    return 0;
}