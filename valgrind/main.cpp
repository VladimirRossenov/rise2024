#include <iostream>
#include <cstdint>

int main()
{
    uint32_t *var1 {new uint32_t {1}};
    *var1 = 5;
    std::cout << *var1 << "\n";
    *var1 = 10;
    std::cout << *var1 << "\n";
    *var1 = 20;

    delete var1;

    var1 = new uint32_t {200};
    std::cout << *var1 << "\n";
    delete var1;

    uint8_t *var2 = new uint8_t {26};
    delete var2;

    return 0;
}