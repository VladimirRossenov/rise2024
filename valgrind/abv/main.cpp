#include <iostream>

void g(void)
{
    new int[1000];
}

void f(void)
{
    new int[500];
    g();
}

int main(void)
{
    int i;
    int** a = new int*[10];

    for (i = 0; i < 10; i++)
    {
        a[i] = new int[250];
    }
    f();

    g();

    for (i = 0; i < 10; i++)
    {
        delete[] a[i];
    }

    delete[] a;
    a = nullptr;

    std::cout << "hello\n";

    return 0;
}