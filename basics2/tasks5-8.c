#include <stdio.h>
#include <stdint.h>

void task5(int *iPtr);
void task6();
void task7();
void task8();

int main()
{
    int i = 5;
    int *iPtr = &i;
    task5(iPtr);
    task6();
    task7();
    task8();
    return 0;
}

void task5(int *iPtr)
{
    *iPtr = -1;
    printf("%d\n", *iPtr);
}

void task6()
{
    struct Bitfield
    {
        uint8_t a : 3;
        uint8_t b : 3;
        uint8_t c : 2;
    } testBit;

    testBit.a = 5;
    testBit.b = 7;
    testBit.c = 2;
    printf("%d, %d, %d\n", testBit.a, testBit.b, testBit.c);
}

void task7()
{
    union IntToFloat
    {
        int iValue;
        float fValue;
    } a;
    a.iValue = 10;
    printf("%d, %.2f\n", a.iValue, (float)a.iValue);
    a.iValue = 0;
    a.fValue = 10.3;
    printf("%d, %.2f\n", (int)a.fValue, a.fValue);
}

void task8()
{
    uint16_t val;
    scanf("%hd", &val);
    // 1111 0000 1111 0000
    printf("%d ", val && (1 >> 15));
    printf("%d\n", (val >> 15) && 1);
}