#include <stdio.h>
#include <stdint.h>

void task9();
void task10();
void task11();
void task12();

int main()
{
    task9();
    task10();
    task11();
    task12();
}

void task9()
{
    enum Seasons
    {
        SPRING = 1,
        SUMMER,
        AUTUMN,
        WINTER
    } seasons;
    printf("%d, %d, %d, %d\n", SPRING, SUMMER, AUTUMN, WINTER);
}

void task10()
{
    int i = 1 << 6;
    printf("Left shift : %d, Right Shift : %d\n", i << 3, i >> 3);
}

void task11()
{
    uint8_t num = 0b11110000;
    uint8_t toPrint = 0b00000000;
    // toPrint = 0b00000111
    for (int i = 3; i > 0; --i)
    {
        toPrint |= (num >> 8 - i);
    }
    printf("%d\n", toPrint);
}

#define ABS(x) ((x > 0) ? (x) : (-x))

void task12()
{
    printf("%d\n", ABS(-100));
}