#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

void task13(int * arr);
void task14();
void task15();

int main()
{
    int *arr = NULL;
    task13(arr);
    free(arr);
    task14();
    task15();
}

void task13(int* arr)
{
    arr = (int *)malloc(10 * sizeof(int));
    if (NULL != arr)
    {
        for (int i = 0; i < 10; ++i)
        {
            *arr = i + 1;
            printf("%d ", *arr);
            arr++;
        }
        printf("\n");
    }
}

void task14()
{
    unsigned long long max = 0xffffffffffffffff;
    printf("%llu, %u, %u, %u\n", max, (unsigned int)max, (unsigned short)max, (uint8_t)max);
}

void task15()
{
    struct Bitfield
    {
        uint16_t a : 8;
        uint16_t b : 5;
        uint16_t c : 3;
    };
    typedef struct Bitfield sBit;
    int toShift = 1;
    sBit *arr = (sBit *)malloc(5 * sizeof(sBit));
    for (int i = 0; i < 5; ++i)
    {
        arr[i].a = i * 9;
        arr[i].b = i * 5;
        arr[i].c = (i + 1) * 1;
        printf("%d, %d, %d\n", arr[i].a, arr[i].b, arr[i].c);
    }
    free(arr);
}
