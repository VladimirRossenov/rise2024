#include <stdio.h>
#include <string.h>

void task1(double *arr);
void task2();
void task3(double *arr, int arrSize, struct Person *strP);
void task4();

struct Person
{
    char name[10];
    int age;
    double weight;
} strPerson;

int main()
{
    double arr[10];
    for (int i = 0; i < 10; ++i)
    {
        scanf("%lf", &arr[i]);
    }

    task1(arr);
    task2();
    task3(arr, 10, &strPerson);
    task4();
}

double average(double *arr, unsigned n)
{
    double result = 0;
    for (int i = 0; i < n; ++i)
    {
        result += *arr;
        *arr++;
    }
    return result / n;
}

void task1(double *arr)
{

    double result = average(arr, 10);
    printf("The average is %.2f\n", result);
}

int getSum(int *a, int *b)
{
    int result = *a + *b;
    return result;
}

void task2()
{
    int a, b;
    scanf("%d%d", &a, &b);
    int sum = getSum(&a, &b);
    printf("The sum is %d\n", sum);
}

void task3(double *arr, int arrSize, struct Person *strP)
{
    for (int i = 0; i < arrSize; ++i)
    {
        *arr += 1;
    }
    strP->age = 24;
    strP->weight = 82;
    strcpy(strP->name, "Vladimir");
    task1(arr);
    printf("%d, %lf, %s\n", strP->age, strP->weight, strP->name);
}

void swap(char *c1, char *c2)
{
    char tmp;
    tmp = *c1;
    *c1 = *c2;
    *c2 = tmp;
}

void permutate(char *str, int start, int end)
{
    int i;
    if (start == end)
        printf("%s  ", str);
    else
    {
        for (i = start; i <= end; i++)
        {
            swap((str + start), (str + i));
            permutate(str, start + 1, end);
            swap((str + start), (str + i));
        }
    }
}

void task4()
{
    char one[] = "one";
    char two[] = "two";
    char three[] = "three";
    permutate(one, 0, 2);
    permutate(two, 0, 2);
    permutate(three, 0, 2);
    printf("\n");
}