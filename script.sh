# cp ./basics/*.c  testDir
directories="$1"
files="$2"


if [ ! -d "$directories" ]; then
    mkdir -p "$directories"
fi

if [ ! -d "$files" ]; then
    mkdir -p "$files"
fi

for item in *;
do
    if [ -d "$item" ]; then
        
        cp -r "$item" "$directories"
    elif [ -f "$item" ]; then
        
        cp "$item" "$files"
    fi 
done

find "$directories" -type f -delete

echo "Files and directories have been copied successfully."
