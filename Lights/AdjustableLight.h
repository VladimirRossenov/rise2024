#ifndef __ADJUSTABLELIGHT_H
#define __ADJUSTABLELIGHT_H
#include "BaseLight.h"

class AdjustableLight : public BaseLight
{
protected:
    double m_brightness;
public:
    AdjustableLight();
    virtual ~AdjustableLight() = default;

    void turnOn(const std::string& lightName) override;
    void setBrightness(const double& brightness) override;
};

#endif