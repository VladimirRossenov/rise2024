#pragma once
#include "ECU.h"

class TrunkECU : public ECU
{
    public:
    
    TrunkECU(LightControlModule* LCM);
    ~TrunkECU();

    void turnOn();
    void turnOff();    
};