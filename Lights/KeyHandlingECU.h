#pragma once
#include "ECU.h"

class KeyHandlingECU : public ECU
{
private:
    void BlinkAllBlinkers();

public:
    KeyHandlingECU(LightControlModule* LCM);
    ~KeyHandlingECU();

    void HandleCarUnlocked();
    void HandleCarLocked();
    void HandleOpenTrunk();
};
