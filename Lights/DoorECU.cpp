#include "DoorECU.h"

DoorECU::DoorECU(LightControlModule* LCM) : ECU(LCM)
{
}

DoorECU::~DoorECU()
{
}

void DoorECU::turnOn()
{
    m_status = 1;
    m_LCM->turnOn(LightCodes::DoorLights);
}
void DoorECU::turnOff()
{
    m_status = 0;
    m_LCM->turnOff(LightCodes::DoorLights);
}