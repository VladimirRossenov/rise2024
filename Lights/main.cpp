#include "LightControlModule.h"
#include "DiagnosticsECU.h"
int main()
{
    LightControlModule* LCM = new LightControlModule();
    std::vector<ECU *> vec;
    BrakeECU bECU(LCM);
    TrunkECU tECU(LCM);
    DoorECU dECU(LCM);
    GloveboxECU gECU(LCM);
    HeadUnitECU hECU(LCM);
    KeyHandlingECU kECU(LCM);
    WheelStickECU wECU(LCM);
    //bECU.turnOn();
    DiagnosticECU diagECU(LCM);
    diagECU.getBulbStatus(LightCodes::StopLights);
    LCM->turnOnAll();
    LCM->turnOff(7);
    LCM->getStatus(7);
    delete LCM;

    // DiagnosticECU diagECU;

    return 0;
}