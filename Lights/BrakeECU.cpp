#include "BrakeECU.h"

BrakeECU::BrakeECU(LightControlModule *LCM) : ECU(LCM)
{
}

BrakeECU::~BrakeECU()
{
}

void BrakeECU::turnOn()
{
    m_status = 1;
    m_LCM->turnOn(LightCodes::StopLights);
}

void BrakeECU::turnOff()
{
    m_status = 0;
    m_LCM->turnOff(LightCodes::StopLights);
}
