#pragma once
#include "ECU.h"

class GloveboxECU : public ECU
{
    public:
    
    GloveboxECU(LightControlModule* LCM);
    ~GloveboxECU();

    void turnOn();
    void turnOff();

};