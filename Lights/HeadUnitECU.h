#pragma once
#include "ECU.h"

class HeadUnitECU : public ECU
{
private:
public:
    HeadUnitECU(LightControlModule* LCM);
    ~HeadUnitECU();
    void warningLightsOn();

    void warningLightsOff();

    void setAmbientLightsColor(Colors color);

    void adjustAmbientBrightness(double brightness);

    void ambientLightsOn();

    void ambientLightsOff();

    void adjustBrightnessCoupe(double brightness);

    void ceilingLightsOn();

    void trunkLightsOn();
};