#include "KeyHandlingECU.h"

KeyHandlingECU::KeyHandlingECU(LightControlModule* LCM) : ECU(LCM)
{
}

KeyHandlingECU::~KeyHandlingECU()
{
}

void KeyHandlingECU::BlinkAllBlinkers()
{
    m_LCM->turnOn(LightCodes::LeftBlinkers);
    m_LCM->turnOn(LightCodes::RightBlinkers);
    m_LCM->turnOff(LightCodes::LeftBlinkers);
    m_LCM->turnOff(LightCodes::RightBlinkers);
}

void KeyHandlingECU::HandleCarLocked()
{
    BlinkAllBlinkers();
    m_LCM->turnOffAll();
}

void KeyHandlingECU::HandleCarUnlocked()
{
    BlinkAllBlinkers();
    m_LCM->turnOn(LightCodes::LowBeam);
}

void KeyHandlingECU::HandleOpenTrunk()
{
    m_LCM->turnOn(LightCodes::TrunkLights);
}