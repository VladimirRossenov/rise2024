#include "AmbientLight.h"

AmbientLight::AmbientLight() : AdjustableLight()
{
    m_color = Colors::red;
}

void AmbientLight::setColor(uint8_t color)
{
    m_color = (Colors)color;
}
