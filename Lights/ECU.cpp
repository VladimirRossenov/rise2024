#include "ECU.h"

ECU::ECU(LightControlModule *LCM)
{
    m_status = 0;
    m_LCM = LCM;
}

ECU::~ECU()
{
   
}

void ECU::turnOn()
{
    m_status = 1;
}

void ECU::turnOff()
{
    m_status = 0;
}

