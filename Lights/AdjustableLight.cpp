#include "AdjustableLight.h"

AdjustableLight::AdjustableLight() : BaseLight()
{
    m_brightness = 100;
}

void AdjustableLight::turnOn(const std::string &lightName)
{
    BaseLight::turnOn(lightName);
    m_brightness = 100;
}

void AdjustableLight::setBrightness(const double &brightness)
{
    m_brightness = brightness;
}
