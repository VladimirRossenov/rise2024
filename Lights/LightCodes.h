#pragma once
#include <cstdint>

enum LightCodes : uint16_t
{
    FogLights = 0,
    DayLights,
    LowBeam,
    HighBeam,
    BackLights,
    LeftBlinkers,
    RightBlinkers,
    StopLights,
    NumberPlateLights,
    DoorLights,
    CeilingLights,
    TrunkLights,
    GloveboxLights,
    FloorLights,
    AmbientLights,
    __ELEMENTCOUNT
};