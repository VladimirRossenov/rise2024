#include "WheelStickECU.h"

WheelStickECU::WheelStickECU(LightControlModule* LCM) : ECU(LCM)
{
}

WheelStickECU::~WheelStickECU()
{
}

void WheelStickECU::dayLightsOn()
{
    m_LCM->turnOn(LightCodes::DayLights);
}

void WheelStickECU::dayLightsOff()
{
    m_LCM->turnOff(LightCodes::DayLights);
}

void WheelStickECU::lowBeamOn()
{
    m_LCM->turnOn(LightCodes::LowBeam);
}

void WheelStickECU::lowBeamOff()
{
    m_LCM->turnOff(LightCodes::LowBeam);
}

void WheelStickECU::highBeamOn()
{
    m_LCM->turnOn(LightCodes::HighBeam);
}

void WheelStickECU::highBeamOff()
{
    m_LCM->turnOff(LightCodes::HighBeam);
}

void WheelStickECU::blinkersLeftOn()
{
    m_LCM->turnOn(LightCodes::LeftBlinkers);
}

void WheelStickECU::blinkersLeftOff()
{
    m_LCM->turnOff(LightCodes::LeftBlinkers);
}

void WheelStickECU::blinkersLeftOnFiveBlinks()
{
    for (int i = 0; i < 5; i++)
    {
        m_LCM->turnOn(LightCodes::LeftBlinkers);
        m_LCM->turnOff(LightCodes::LeftBlinkers);
    }
}

void WheelStickECU::blinkersRightOnFiveBlinks()
{
    for (int i = 0; i < 5; i++)
    {
        m_LCM->turnOn(LightCodes::RightBlinkers);
        m_LCM->turnOff(LightCodes::RightBlinkers);
    }
}

void WheelStickECU::blinkersRightOn()
{
    m_LCM->turnOn(LightCodes::RightBlinkers);
}

void WheelStickECU::blinkersRightOff()
{
    m_LCM->turnOff(LightCodes::RightBlinkers);
}
