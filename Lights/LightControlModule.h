#ifndef __LIGHTCONTROLMODULE_H
#define __LIGHTCONTROLMODULE_H

#include "AdjustableLight.h"
#include "AmbientLight.h"
#include "LightCodes.h"
#include <vector>

class LightControlModule
{
private:
    std::vector<BaseLight *> lights;
    std::string getLightName(uint16_t lightCode);

public:
    LightControlModule();
    LightControlModule(const LightControlModule &other) = delete;
    LightControlModule &operator=(const LightControlModule &other) = default;

    ~LightControlModule();

    void turnOn(uint16_t lightCode);
    void turnOff(uint16_t lightCode);
    void turnOnAll();
    void turnOffAll();

    void turnOnWarningLights();
    void turnOffWarningLights();
    void setAmbientLightsColor(uint8_t color);
    void adjustBrightness(uint16_t light, const double &brightness);

    void getStatus(uint16_t lightCode);
    void setHealth(uint16_t lightCode);
    void getBulbStatus(uint16_t lightCode);
};

#endif
