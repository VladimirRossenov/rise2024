#include "TrunkECU.h"

TrunkECU::TrunkECU(LightControlModule* LCM) : ECU(LCM)
{
}

TrunkECU::~TrunkECU()
{
}

void TrunkECU::turnOn()
{
    m_status = 1;
    m_LCM->turnOn(LightCodes::TrunkLights);
}
void TrunkECU::turnOff()
{
    m_status = 0;
    m_LCM->turnOff(LightCodes::TrunkLights);
}