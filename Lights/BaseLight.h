#ifndef __BASELIGHT_H
#define __BASELIGHT_H

#include <iostream>
#include <string>

class BaseLight
{
protected:
    double m_health;
    bool m_status;
    bool m_hasBulb;

public:
    BaseLight();
    ~BaseLight() = default;

    virtual void turnOn(const std::string &lightName);
    virtual void setBrightness(const double &brightness) {};
    virtual void setColor(uint8_t color) {};
    void turnOff();

    bool isLightOn() const;
    void getStatus() const;
    void isBulbBroken() const;
    void changeBulb();
};

#endif