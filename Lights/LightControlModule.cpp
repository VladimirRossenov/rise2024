#include "LightControlModule.h"

std::string LightControlModule::getLightName(uint16_t lightCode)
{
    std::string result;
    switch (lightCode)
    {
    case 0:
        result = "FogLights";
        break;
    case 1:
        result = "DayLights";
        break;
    case 2:
        result = "LowBeam";
        break;
    case 3:
        result = "HighBeam";
        break;
    case 4:
        result = "BackLights";
        break;
    case 5:
        result = "LeftBlinkers";
        break;
    case 6:
        result = "RightBlinkers";
        break;
    case 7:
        result = "StopLights";
        break;
    case 8:
        result = "NumberPlateLights";
        break;
    case 9:
        result = "CeilingLights";
        break;
    case 10:
        result = "TrunkLights";
        break;
    case 11:
        result = "GloveboxLights";
        break;
    case 12:
        result = "AmbientLights";
        break;
    case 13:
        result = "DoorLights";
        break;
    case 14:
        result = "FloorLights";
        break;
    }
    return result;
}

LightControlModule::LightControlModule()
{
    for(int i = 0; i < 9; ++i){
        lights.push_back(new BaseLight);
    }
    for(int i = 0; i < 4; ++i){
        lights.push_back(new AdjustableLight);
    }
    lights.push_back(new AmbientLight);
}

LightControlModule::~LightControlModule()
{
    for (unsigned i = 0; i < lights.size(); ++i)
    {
        delete lights[i];
    }
}

void LightControlModule::turnOn(uint16_t lightCode)
{
    if (lights[LightCodes::LeftBlinkers]->isLightOn() && lightCode == LightCodes::RightBlinkers)
    {
        lights[LightCodes::LeftBlinkers]->turnOff();
    }
    else if (lights[LightCodes::RightBlinkers]->isLightOn() && lightCode == LightCodes::LeftBlinkers)
    {
        lights[LightCodes::RightBlinkers]->turnOff();
    }
    lights[lightCode]->turnOn(getLightName(lightCode));
}

void LightControlModule::turnOff(uint16_t lightCode)
{
    lights[lightCode]->turnOff();
}

void LightControlModule::turnOnAll()
{
    for (unsigned i = 0; i < LightCodes::__ELEMENTCOUNT - 1; ++i)
    {
        lights[i]->turnOn(getLightName(i));
    }
}

void LightControlModule::turnOffAll()
{
    for (unsigned i = 0; i < LightCodes::__ELEMENTCOUNT; ++i)
    {
        lights[i]->turnOn(getLightName(i));
    }
}

void LightControlModule::turnOnWarningLights()
{
    lights[LightCodes::LeftBlinkers]->turnOn(getLightName(LightCodes::LeftBlinkers));
    lights[LightCodes::RightBlinkers]->turnOn(getLightName(LightCodes::RightBlinkers));
}

void LightControlModule::turnOffWarningLights()
{
    lights[LightCodes::LeftBlinkers]->turnOff();
    lights[LightCodes::RightBlinkers]->turnOff();
}

void LightControlModule::setAmbientLightsColor(uint8_t color)
{
    lights[LightCodes::AmbientLights]->setColor(color);
}

void LightControlModule::adjustBrightness(uint16_t light, const double &brightness)
{
    lights[light]->setBrightness(brightness);
}

void LightControlModule::getStatus(uint16_t lightCode)
{
    std::cout << getLightName(lightCode) << ' ';
    lights[lightCode]->getStatus();
}

void LightControlModule::setHealth(uint16_t lightCode)
{
    lights[lightCode]->changeBulb();
}

void LightControlModule::getBulbStatus(uint16_t lightCode)
{
    std::cout << getLightName(lightCode) << ": ";
    lights[lightCode]->isBulbBroken();
}
