#include "DiagnosticsECU.h"

DiagnosticECU::DiagnosticECU(LightControlModule *LCM) : ECU(LCM)
{
}

DiagnosticECU::~DiagnosticECU()
{
    // for (unsigned i = 0; i < m_ecus.size(); ++i)
    // {
    //     delete m_ecus[i];
    // }
}

// void DiagnosticECU::addECUS(std::vector<ECU *> &&ECUs)
// {
//     m_ecus = std::move(ECUs);
// }
// void DiagnosticECU::LightsTest()
// {
//     for (auto &el : m_ecus)
//     {
//          el->lightTest();
//     }
// }
void DiagnosticECU::getInfoForLight(LightCodes light)
{
    m_LCM->getStatus(light);
}
void DiagnosticECU::getInfoForAllLights()
{
    for (int i = 0; i < LightCodes::__ELEMENTCOUNT; i++)
    {
        m_LCM->getStatus(i);
    }
}
void DiagnosticECU::setBulbHealth(LightCodes light)
{
    m_LCM->setHealth(light);
}
void DiagnosticECU::getBulbStatus(LightCodes light)
{
    m_LCM->getBulbStatus(light);
}
void DiagnosticECU::setAllBulbHealth()
{
    for (int i = 0; i < LightCodes::__ELEMENTCOUNT; i++)
    {
        m_LCM->setHealth(i);
    }
}
void DiagnosticECU::getBulbStatusAll()
{
    for (int i = 0; i < LightCodes::__ELEMENTCOUNT; i++)
    {
        m_LCM->getBulbStatus(i);
    }
}
