#include "HeadUnitECU.h"

HeadUnitECU::HeadUnitECU(LightControlModule* LCM) : ECU(LCM)
{
}

HeadUnitECU::~HeadUnitECU()
{
}

void HeadUnitECU::warningLightsOn()
{
    m_LCM->turnOnWarningLights();
}

void HeadUnitECU::warningLightsOff()
{
    m_LCM->turnOffWarningLights();
}

void HeadUnitECU::setAmbientLightsColor(Colors color)
{
    m_LCM->setAmbientLightsColor(color);
}

void HeadUnitECU::adjustAmbientBrightness(double brightness)
{
    m_LCM->adjustBrightness(15, brightness);
}

void HeadUnitECU::ambientLightsOn()
{
    m_LCM->turnOn(15);
}

void HeadUnitECU::ambientLightsOff()
{
    m_LCM->turnOff(15);
}

void HeadUnitECU::adjustBrightnessCoupe(double brightness)
{
    for (int i = 11; i < 15; i++)
    {
        m_LCM->adjustBrightness(i, brightness);
    }
}

void HeadUnitECU::ceilingLightsOn()
{
    m_LCM->turnOn(11);
}

void HeadUnitECU::trunkLightsOn()
{
    m_LCM->turnOn(12);
}
