#pragma once
#include "ECU.h"

class DoorECU : public ECU
{
public:
    DoorECU(LightControlModule* LCM);
    ~DoorECU();

    void turnOn();
    void turnOff();
};