#include "GloveboxECU.h"

GloveboxECU::GloveboxECU(LightControlModule* LCM) : ECU(LCM)
{
}

GloveboxECU::~GloveboxECU()
{
}

void GloveboxECU::turnOn()
{
    m_status = 1;
    m_LCM->turnOn(LightCodes::GloveboxLights);
}

void GloveboxECU::turnOff()
{
    m_status = 0;
    m_LCM->turnOff(LightCodes::GloveboxLights);
}