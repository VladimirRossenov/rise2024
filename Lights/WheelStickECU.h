#pragma once
#include "ECU.h"

class WheelStickECU : public ECU
{
private:
public:
    WheelStickECU(LightControlModule* LCM);
    ~WheelStickECU();

    void dayLightsOn();
    void dayLightsOff();
    void lowBeamOn();
    void lowBeamOff();
    void highBeamOn();
    void highBeamOff();
    void blinkersLeftOn();
    void blinkersLeftOff();
    void blinkersLeftOnFiveBlinks();
    void blinkersRightOnFiveBlinks();
    void blinkersRightOn();
    void blinkersRightOff();
};
