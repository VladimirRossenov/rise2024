#pragma once
#include "LightControlModule.h"
#include "LightCodes.h"
class ECU
{
protected:
    bool m_status;
    LightControlModule *m_LCM;

public:
    virtual void turnOn();
    virtual void turnOff();
    ECU(LightControlModule *LCM);
    virtual ~ECU();

    // void setLCM(LightControlModule* _LCM);
};