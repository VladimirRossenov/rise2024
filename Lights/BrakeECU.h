#pragma once
#include "ECU.h"

class BrakeECU : public ECU
{
    public:
    
    BrakeECU(LightControlModule *LCM);
    ~BrakeECU();

    void turnOn();
    void turnOff();
};