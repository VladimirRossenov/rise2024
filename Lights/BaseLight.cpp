#include "BaseLight.h"

BaseLight::BaseLight()
{
    m_health = 100;
    m_status = 0;
    m_hasBulb = 1;
}

void BaseLight::turnOn(const std::string &lightName)
{
    m_health -= 0.1;
    if (m_health == 0)
    {
        m_hasBulb = 0;
        m_status = 0;
    }
    else
    {
        m_status = 1;
        std::cout << lightName << ": im on!\n";
    }
}

void BaseLight::turnOff()
{
    m_status = 0;
}

bool BaseLight::isLightOn() const
{
    return m_status;
}

void BaseLight::getStatus() const
{
    std::cout << "Light Status: " << m_status << '\n'
              << "Bulb health: " << m_health << '\n'
              << "Is Bulb broken? " << !m_hasBulb << '\n';
}

void BaseLight::isBulbBroken() const
{
    std::cout << "Is Bulb Broken? " << !m_hasBulb << '\n';
}

void BaseLight::changeBulb()
{
    m_hasBulb = 1;
    m_health = 100;
}
