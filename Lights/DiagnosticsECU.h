#pragma once
#include <iostream>
#include <vector>
#include "BrakeECU.h"
#include "DoorECU.h"
#include "GloveboxECU.h"
#include "HeadUnitECU.h"
#include "KeyHandlingECU.h"
#include "TrunkECU.h"
#include "WheelStickECU.h"

class DiagnosticECU : public ECU
{
private:
    //std::vector<ECU *> m_ecus;

public:
    DiagnosticECU(LightControlModule* LCM);
    DiagnosticECU(const DiagnosticECU &other) = delete;
    DiagnosticECU &operator=(const DiagnosticECU &other) = delete;
    ~DiagnosticECU();

    //void addECUS(std::vector<ECU *> &&ECUs);

    void LightsTest();
    void getInfoForLight(LightCodes light);
    void getInfoForAllLights();
    void setBulbHealth(LightCodes light);
    void getBulbStatus(LightCodes light);
    void setAllBulbHealth();
    void getBulbStatusAll();
};