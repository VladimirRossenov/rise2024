#ifndef __AMBIENTLIGHT_H
#define __AMBIENTLIGHT_H
#include "AdjustableLight.h"

enum Colors : uint8_t
{
    red = 0,
    green,
    blue,
    yellow,
    orange,
    purple
};

class AmbientLight : public AdjustableLight
{
private:
    Colors m_color;
public:
    AmbientLight();
    ~AmbientLight() = default;
    
    void setColor(uint8_t color) override;    
};

#endif