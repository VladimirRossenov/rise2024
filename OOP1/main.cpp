#include "Book.h"
#include "Magazine.h"

int main()
{
    Book book("C++ OOP", 2024, "C++ OOP");
    book.display();
    
    Magazine magazine("FirstEdition", 2024, 1);
    magazine.display();

    book.~Book();
    magazine.display();
}