#include "Book.h"

Book::Book() : LibraryItem(), name("")
{
}

Book::Book(const std::string &title, int year, const std::string &name) : LibraryItem(title, year), name(name)
{
}

Book::~Book()
{
}

void Book::display() const
{
    std::cout << "Item Count : " << counter << " Title : " << title << ", year: " << year << " ,Author : " << name << '\n';
}
