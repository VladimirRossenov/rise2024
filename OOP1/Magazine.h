#pragma once
#include "LibraryItem.h"

class Magazine : LibraryItem
{
private:
    int issue;
public:
    Magazine();
    Magazine(const std::string& title, int year, int issue);

    ~Magazine();

    void display() const override;
};

