#include "Magazine.h"

Magazine::Magazine() : LibraryItem(), issue(0)
{
}

Magazine::Magazine(const std::string &title, int year, int issue) : LibraryItem(title, year), issue(issue)
{
}

Magazine::~Magazine()
{
}

void Magazine::display() const
{
    std::cout << "Item Count : " << counter << " Title : " << title << ", year: " << year << ", Issue : " << issue  << '\n';
}