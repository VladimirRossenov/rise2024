#pragma once
#include "LibraryItem.h"

class Book : public LibraryItem
{
private:
    std::string name;

public:
    Book();
    Book(const std::string &name, int year, const std::string &title);
    ~Book();

    void display() const override;
};

