#pragma once
#include <iostream>
#include <string>

class LibraryItem
{
protected:
    std::string title;
    int year;
public:
    static int counter;
    LibraryItem();
    LibraryItem(const std::string& title, int year);
    LibraryItem(const LibraryItem& other);
    virtual ~LibraryItem();

    virtual void display() const;
    static int getItemCount();
};

