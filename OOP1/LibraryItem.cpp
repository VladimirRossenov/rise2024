#include "LibraryItem.h"

int LibraryItem::counter = 0;

LibraryItem::LibraryItem() : title(""), year(0)
{
    ++counter;
}

LibraryItem::LibraryItem(const std::string &title, int year) : title(title), year(year)
{
    ++counter;
}

LibraryItem::LibraryItem(const LibraryItem &other) : title(other.title), year(other.year)
{
    ++counter;
}

LibraryItem::~LibraryItem()
{
    --counter;
}

void LibraryItem::display() const
{
    std::cout << "Item Count : " << counter << " Title : " << title << ", year: " << year << '\n';
}

int LibraryItem::getItemCount()
{
    return counter;
}