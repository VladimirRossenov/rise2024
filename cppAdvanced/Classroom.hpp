#include <iostream>
#include <string>
#include <map>
#include <set>
#include <cstdint>

enum EnumGrades : uint8_t
{
    F = 2,
    D,
    C,
    B,
    A
};

struct Student
{
    std::string _name;
    uint8_t _age;
    std::map<std::string, EnumGrades> _grades;
    Student() : _name(""), _age(0){};
    Student(const std::string &name, const uint8_t age) : _name(name), _age(age)
    {
        _grades["Math"] = EnumGrades::A;
        _grades["Informatics"] = EnumGrades::A;
        _grades["Chemistry"] = EnumGrades::C;
    }
    Student(const Student &other) : _name(other._name), _age(other._age), _grades(other._grades)
    {
    }

    void printStudent() const
    {
        std::cout << "Name: " << _name << ", age: " << (int)_age << ", grades: \n";
        for (const auto &subject : _grades)
        {
            std::cout << subject.first << "-" << (int)subject.second << '\n';
        }
    }
};

class Classroom
{
private:
    std::set<Student, bool (*)(const Student &, const Student &)> m_StudentList;

public:
    Classroom() : m_StudentList([](const Student &a, const Student &b)
                                { return a._name < b._name; }) {}
    void addStudentRef(const Student &student);
    void addStudentCopy(Student student);
    void addStudentMove(Student &&student);
    void removeStudent(const int index);
    void printAllStudentData() const;
};

inline void Classroom::addStudentRef(const Student &st)
{
    // st._grades = {std::make_pair("Math", EnumGrades::A),
    //               std::make_pair("Informatics", EnumGrades::A),
    //               std::make_pair("Physics", EnumGrades::B),
    //               std::make_pair("Chemistry", EnumGrades::C)
    //               };

    // m_StudentList.insert(st);

    m_StudentList.emplace(st);
}

inline void Classroom::addStudentCopy(Student st)
{
    m_StudentList.emplace(st);
}

inline void Classroom::addStudentMove(Student &&st)
{
    m_StudentList.emplace(st);
}

inline void Classroom::removeStudent(const int index)
{
    auto it = std::next(m_StudentList.begin(), index);
    if (m_StudentList.end() != it)
    {
        m_StudentList.erase(it);
    }
}

inline void Classroom::printAllStudentData() const
{
    for (const auto &student : m_StudentList)
    {
        student.printStudent();
    }
}
