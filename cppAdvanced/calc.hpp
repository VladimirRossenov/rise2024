#include <iostream>

template <typename T>
T sum(const T &n1, const T &n2)
{
    return n1 + n2;
}

template <typename T>
T subtract(const T &n1, const T &n2)
{
    return n1 - n2;
}

template <typename T>
T divide(const T &n1, const T &n2)
{
    return n1 / n2;
}

template <typename T>
T multiply(const T &n1, const T &n2)
{
    return n1 * n2;
}

template <typename T>
class Calculator
{
private:
    T num1;
    T num2;
public:
    Calculator(const T &n1, const T &n2) : num1(n1), num2(n2) {}

    void displayResult(char op)
    {
        switch (op)
        {
        case '+':
            std::cout << sum<T>(num1, num2);
            break;
        case '-':
            std::cout << subtract<T>(num1, num2);
            break;
        case '/':
            std::cout << divide<T>(num1, num2);
            break;
        case '*':
            std::cout << multiply<T>(num1, num2);
            break;
        default:
            std::cout << "No such operation.\n";
            break;
        }
        std::cout << '\n';
    }
};