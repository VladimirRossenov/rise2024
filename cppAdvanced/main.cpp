#include <iostream>
// #define __Calc
#ifdef __Calc

#include "calc.hpp"

int main()
{
    Calculator<int> calc(5, 10);
    calc.displayResult('+');
    calc.displayResult('-');
    calc.displayResult('/');
    calc.displayResult('*');
}

#endif

#define __Classroom.hpp
#ifdef __Classroom.hpp

#include "Classroom.hpp"
#include <chrono>
#include <vector>
#include <iomanip>

Student st = {"Vladimir", 24};
Student st1("Ivan", 22);
Student st2("Dimitur", 19);
Student st3("st3", 29);
Student st4("st4", 12);
Student st5("st5", 197);
Student st6("st6", 190);
Student st7("st7", 150);
Student st8("st", 14);
Student st9("st", 10);

void addStudentsByRef(Classroom *room);
void addStudentsByCopy(Classroom *room);
void addStudentsByMove(Classroom *room);
void addStudentsVec(std::vector<Student> *vec);
void addStudentsVecE(std::vector<Student> *vec);
int main()
{
    Classroom room;
    Classroom room1;
    Classroom room2;
    std::vector<Student> vec;
    std::vector<Student> vec1;

    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    start = std::chrono::high_resolution_clock::now();
    addStudentsByCopy(&room);
    end = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double> elapsedTime(end - start);
    double seconds(elapsedTime.count());
    std::cout << "Copy test: \n"
              << std::fixed
              << seconds << '\n';

    start = std::chrono::high_resolution_clock::now();
    addStudentsByRef(&room1);
    end = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double> elapsedTime2(end - start);
    double seconds2(elapsedTime2.count());
    std::cout << "Ref test : \n"
              << std::fixed
              << seconds2 << '\n';

    start = std::chrono::high_resolution_clock::now();
    addStudentsByMove(&room2);
    end = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double> elapsedTime3(end - start);
    double seconds3(elapsedTime3.count());
    std::cout << "Move test : \n"
              << std::fixed
              << seconds3 << '\n';

    start = std::chrono::high_resolution_clock::now();
    addStudentsVec(&vec);
    end = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double> elapsedTime4(end - start);
    double seconds4(elapsedTime4.count());
    std::cout << "Vec test ins : \n"
              << std::fixed
              << seconds3 << '\n';

    start = std::chrono::high_resolution_clock::now();
    addStudentsVecE(&vec1);
    end = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double> elapsedTime5(end - start);
    double seconds5(elapsedTime5.count());
    std::cout << "Vec test empl : \n"
              << std::fixed
              << seconds3 << '\n';
}

void addStudentsByCopy(Classroom *room)
{
    room->addStudentCopy(st);
    room->addStudentCopy(st1);
    room->addStudentCopy(st2);
    room->addStudentCopy(st3);
    room->addStudentCopy(st4);
    room->addStudentCopy(st5);
    room->addStudentCopy(st6);
    room->addStudentCopy(st7);
    room->addStudentCopy(st8);
    room->addStudentCopy(st9);
}

void addStudentsByRef(Classroom *room)
{
    room->addStudentRef(st);
    room->addStudentRef(st1);
    room->addStudentRef(st2);
    room->addStudentRef(st3);
    room->addStudentRef(st4);
    room->addStudentRef(st5);
    room->addStudentRef(st6);
    room->addStudentRef(st7);
    room->addStudentRef(st8);
    room->addStudentRef(st9);
}

void addStudentsByMove(Classroom *room)
{
    room->addStudentMove(std::move(st));
    room->addStudentMove(std::move(st1));
    room->addStudentMove(std::move(st2));
    room->addStudentMove(std::move(st3));
    room->addStudentMove(std::move(st4));
    room->addStudentMove(std::move(st5));
    room->addStudentMove(std::move(st6));
    room->addStudentMove(std::move(st7));
    room->addStudentMove(std::move(st8));
    room->addStudentMove(std::move(st9));
}

void addStudentsVec(std::vector<Student> *vec)
{
    vec->push_back(st);
    vec->push_back(st1);
    vec->push_back(st2);
    vec->push_back(st3);
    vec->push_back(st4);
    vec->push_back(st5);
    vec->push_back(st6);
    vec->push_back(st7);
    vec->push_back(st8);
    vec->push_back(st9);
}

void addStudentsVecE(std::vector<Student> *vec)
{
    vec->emplace_back(st);
    vec->emplace_back(st1);
    vec->emplace_back(st2);
    vec->emplace_back(st3);
    vec->emplace_back(st4);
    vec->emplace_back(st5);
    vec->emplace_back(st6);
    vec->emplace_back(st7);
    vec->emplace_back(st8);
    vec->emplace_back(st9);
}

#endif
